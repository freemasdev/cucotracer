import logging
import inspect
import types

logging.basicConfig(filename='cucotracer.log', level=logging.INFO, format='%(message)s')

#  format_args taken from https://gist.github.com/beng/7817597
def format_args(args, kwargs):
    """
    makes a nice string representation of all the arguments
    """

    allargs = []
    for item in args:
        allargs.append('%s' % str(item))

    for key, item in kwargs.items():
        allargs.append('%s=%s' % (key, str(item)))

    formattedArgs = '(%s)' % ', '.join(allargs)

    if len(formattedArgs) > 150:
        return formattedArgs[:146] + " ..."
    return formattedArgs




def cuco(*content):
    if isinstance(content[0], types.FunctionType) or isinstance(content[0], types.MethodType) :
        content=content[0]
        def wrapper(*args, **kwargs):
            stack = inspect.stack()
            try:
                the_class = str(stack[1][0].f_locals["self"].__class__).replace('__main__.','')+' '
                the_method = stack[1][0].f_code.co_name
                method_from = '''group %s
    ''' % (the_method)
                end_method = '''
end'''
            except:
                the_class = '['
                the_method = '\b'
                method_from = ''
                end_method = ''
            try:
                the_class2 = ' '+str(args[0].__class__).replace('__main__.','')
                the_method2 = content.__name__
            except:
                the_class2 = '__main__'
                the_method2 = content.__name__


       #logging.info("%s%s -> %s: %s%s%s" % (method_from, the_class, the_class2, the_method2, args[1:], end_method)) 
            logging.info("%s->%s%s: %s%s" % (the_class, 'o' if the_method2 == '__init__' else '', the_class2, the_method2, format_args(args[1:],kwargs))) 
            return content(*args, **kwargs)
    else:
        orig_class=content[0]
        wrapper=orig_class
        for name, m in inspect.getmembers(orig_class, inspect.ismethod):
            if not name.startswith('__'):
                setattr(wrapper, name, cuco(m))
            elif name == '__init__':
                setattr(wrapper, name, cuco(m))
    return wrapper


