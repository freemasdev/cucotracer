================
CUte COde TRACER
================

About
=====

Have you ever dreamed with automagically plotting method calls in your code by just running it? Wouldn't be that really cool for tracing your code? 

Here you are a damn simple tool for that!

Requirements
============

* plantuml_

.. _plantuml: https://pypi.python.org/pypi/plantuml

Usage
=====

Just decorate your methods, functions or classes:

.. code:: python

    import sys
    sys.path.append('..')
    from cucotracer import cuco
    import random

    @cuco
    class Dice():
      '''
      Just a random point in segment [0,radius]
      '''
      def __init__(self, radius=1):
	self.radius = radius

      def roll(self):
	return random.random()*self.radius

      def rpoint(self):
	return Point(self.roll(), self.roll())

    @cuco
    class Circle():
      '''
      A circle centered in (0,0) with radius r
      '''
      def __init__(self, radius=1):
	self.radius = radius
	self.__inside=0.
	self.__total=0.
	self.dice=Dice(self.radius)

      def is_inside(self, point):
	return  point.x**2 + point.y**2 < self.radius**2

      def shot(self, point):
	self.__total+=1
	if self.is_inside(point):
	  self.__inside+=1

      def rshot(self, n=1):
	for i in range(n):
	  self.shot(self.dice.rpoint())


      @property
      def pi(self):
	return 4*self.__inside/self.__total

    @cuco
    class Point():
      def __init__(self, x, y):
	self.x = x
	self.y = y

      def __str__(self):
	return '(%s, %s)' % (self.x, self.y)

    c=Circle(1)
    for i in range(2):
      c.rshot(2)

And run your code. A cucotrace.log file will be generated

.. code::

    [->o Circle: __init__(1)
    Circle ->o Dice: __init__(1)
    [-> Circle: rshot(2)
    Circle -> Dice: rpoint()
    Dice -> Dice: roll()
    Dice -> Dice: roll()
    Dice ->o Point: __init__(0.0363715757103, 0.152580532437)
    Circle -> Circle: shot((0.0363715757103, 0.152580532437))
    Circle -> Circle: is_inside((0.0363715757103, 0.152580532437))
    Circle -> Dice: rpoint()
    Dice -> Dice: roll()
    Dice -> Dice: roll()
    Dice ->o Point: __init__(0.639702020369, 0.414858221365)
    Circle -> Circle: shot((0.639702020369, 0.414858221365))
    Circle -> Circle: is_inside((0.639702020369, 0.414858221365))
    [-> Circle: rshot(2)
    Circle -> Dice: rpoint()
    Dice -> Dice: roll()
    Dice -> Dice: roll()
    Dice ->o Point: __init__(0.171367307299, 0.852536183572)
    Circle -> Circle: shot((0.171367307299, 0.852536183572))
    Circle -> Circle: is_inside((0.171367307299, 0.852536183572))
    Circle -> Dice: rpoint()
    Dice -> Dice: roll()
    Dice -> Dice: roll()
    Dice ->o Point: __init__(0.987241529563, 0.407164821854)
    Circle -> Circle: shot((0.987241529563, 0.407164821854))
    Circle -> Circle: is_inside((0.987241529563, 0.407164821854))

And you will be able to plot it!

   python -m plantuml cucotracer.log


.. image:: https://bytebucket.org/freemasdev/cucotracer/raw/dd2bed75f0a5131a5dc1d01afb19254252fb5de7/doc/cucotracer.png